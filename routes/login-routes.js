const express = require("express");
const router = express.Router();
const multipart = require("connect-multiparty");
const multipartMiddleware = multipart();
const userDao = require("../modules/users-dao.js");


router.post("/api/login", multipartMiddleware, async function (req, res) {

    const email = req.body.email;
    const password = req.body.password;

    const user = await userDao.retrieveUserWithCredentials(email, password);

    if (user) {
        res.json({
            "user": {
                "id": user.id,
                "email": user.email,
            },
            "token": {
                "token": user.token,
            }
        })
    }

    else {
        res.json(401, { error: 'email or password does not match our records' })
    }
    
});


module.exports = router;
