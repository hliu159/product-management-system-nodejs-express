const express = require("express");
const router = express.Router();
const productsDao = require("../modules/products-dao.js");
const userDao = require("../modules/users-dao.js");
const upload = require("../modules/multer-uploader.js");
const fs = require("fs");
const uuid = require('uuid');
const path = require('path');

router.get("/api/products", async function (req, res) {
  const products = await productsDao.retrieveAllproducts();
  res.send(products)
  })

  router.get("/storage/images/:path", async function(req, res){
    const path = req.params.path;
    const filePath = "./public/images/"+path;

    try {
        const cs = fs.createReadStream(filePath);

        cs.on("error", (err) => { 
            console.error("Error reading stream:", err);
            res.status(500).end(); 
        });

        cs.on("data", chunk => {
            res.write(chunk);
        });

        cs.on("end", () => {
            res.status(200);
            res.end();
        });

    } catch (err) {
        console.error("Error accessing file:", err);
        if (err.code === 'ENOENT') {
            // File not found
            res.status(404).send("File not found");
        } else if (err.code === 'EACCES') {
            // Permission denied
            res.status(403).send("Permission denied");
        } else {
            // Other errors
            res.status(500).send("Internal server error");
        }
    }
});


router.post("/api/products", upload.single("product_image"), async function (req,res){
  const category_id = 1;
  const title = req.body.title;
  const description = req.body.description;
  const price = req.body.price;
  const is_active = 1;

  if(req.file){
    const fileInfo = req.file;
    const oldFileName = fileInfo.path;
    const fileExtension = path.extname(fileInfo.originalname);
    const randomName = uuid.v4();
    const newFileName = `./public/images/${randomName}${fileExtension}`; 
    fs.renameSync(oldFileName, newFileName);
    const product_image = newFileName.replace("./public/", "");
    await productsDao.createNewProduct(category_id,title,description,price,product_image,is_active)
  } else {
    const product_image = "";
    await productsDao.createNewProduct(category_id,title,description,price,product_image,is_active)
  }

  const product = await productsDao.retrieveNewProduct()
  res.json(product[0])
  })

  router.post("/api/product/:id", upload.single("product_image"), async function(req,res){
    const id = req.params.id;
    const category_id = 1;
    const title = req.body.title;
    const description = req.body.description;
    const price = req.body.price;
    const is_active = 1;
  
    if(req.file){
      const fileInfo = req.file;
      const oldFileName = fileInfo.path;
      const fileExtension = path.extname(fileInfo.originalname);
      const randomName = uuid.v4();
      const newFileName = `./public/images/${randomName}${fileExtension}`; 
      fs.renameSync(oldFileName, newFileName);

      const product_image = newFileName.replace("./public/", "");
      await productsDao.editProduct(id,category_id,title,description,price,product_image,is_active)
    } else {
      await productsDao.editProductNoPic(id,category_id,title,description,price,is_active)
    }
    
    let data = await productsDao.retrieveProductByID(id)
    res.json(data[0])
  })

router.delete('/api/product/:id', async function (req,res){
    const id = req.params.id;
    const user = await userDao.retrieveUserWithToken(req.headers.token);
    if (user){
      await productsDao.deleteProductByID(id);
      res.send("true")
    } else {
      res.send("No Auth")
    } 
})

module.exports = router;