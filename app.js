// Setup Express
const express = require("express");
const app = express();
const port = 8000;

const cors = require('cors');
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,   //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));

// Setup body-parser
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Setup routes
const loginRouter = require("./routes/login-routes.js");
app.use(loginRouter);

const productsRouter = require("./routes/products-routes.js");
app.use(productsRouter);

// Start the server running. Once the server is running, the given function will be called, which will
// log a simple message to the server console. Any console.log() statements in your node.js code
// can be seen in the terminal window used to run the server.
app.listen(port, function () {
    console.log(`App listening on port ${port}!`);
});