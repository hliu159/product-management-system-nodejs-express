Product Management System (Back-end, NodeJS + Express)

This is the server side of the product management system.

Build by NodeJS, Express and SQLite

Instruction:
`npm install` to install packages that the project depends on
`node app` to run the server on http://localhost:8000
