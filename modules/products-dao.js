const dbPromise = require("./database.js");

// retrieve products
async function retrieveAllproducts() {
    const db = await dbPromise.getConnection();
    const [allProducts] = await db.query(`SELECT * FROM products`);
    db.release();
    return allProducts;
}

async function retrieveProductByID(id) {
    const db = await dbPromise.getConnection();
    const [product] = await db.query(`SELECT * FROM products WHERE id = ?`, [id]);
    db.release();
    return product;
}

async function retrieveNewProduct() {
    const db = await dbPromise.getConnection();
    const [product] = await db.query(`SELECT * FROM products ORDER BY id DESC LIMIT 1`);
    db.release();
    return product;
}

// create, edit, delete products
async function createNewProduct(category_id, title, description, price, product_image, is_active) {
    const db = await dbPromise.getConnection();
    const [newProduct] = await db.execute(
        `INSERT INTO products (category_id, title, description, price, product_image, is_active) VALUES (?, ?, ?, ?, ?, ?)`,
        [category_id, title, description, price, product_image, is_active]
    );
    db.release();
    return newProduct;
}

async function editProduct(id, category_id, title, description, price, product_image, is_active) {
    const db = await dbPromise.getConnection();
    await db.execute(
        `UPDATE products SET category_id = ?, title = ?, description = ?, price = ?, product_image = ?, is_active = ? WHERE id = ?`,
        [category_id, title, description, price, product_image, is_active, id]
    );
    db.release();
}

async function editProductNoPic(id, category_id, title, description, price, is_active) {
    const db = await dbPromise.getConnection();
    await db.execute(
        `UPDATE products SET category_id = ?, title = ?, description = ?, price = ?, is_active = ? WHERE id = ?`,
        [category_id, title, description, price, is_active, id]
    );
    db.release();
}

async function deleteProductByID(id) {
    const db = await dbPromise.getConnection();
    const [deleteProduct] = await db.execute(`DELETE FROM products WHERE id = ?`, [id]);
    db.release();
    return deleteProduct;
}

// Export functions
module.exports = {
    retrieveAllproducts,
    retrieveProductByID,
    retrieveNewProduct,
    createNewProduct,
    editProduct,
    editProductNoPic,
    deleteProductByID
};