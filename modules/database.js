const mysql = require("mysql2/promise");

// MySQL configuration
const mysqldb_config = {
    host: "46.250.244.212",
    user: "dbuser",
    password: "MI4m481OuUJ1D9KijI921KFMRFHndvNi",
    database: "olympus-mons-dev"
};

// Create a MySQL connection pool
const dbPromise = mysql.createPool(mysqldb_config);

// Export the database connection
module.exports = dbPromise;