const dbPromise = require("./database.js");

async function retrieveUserWithCredentials(email, password) {
    const db = await dbPromise.getConnection();

    // Using parameterized query to prevent SQL injection
    const [user] = await db.query(
        `SELECT * FROM users WHERE email = ? AND password = ?`,
        [email, password]
    );

    db.release();
    return user[0];  // Return the first matching user or undefined if none found
}

async function retrieveUserWithToken(token) {
    const db = await dbPromise.getConnection();

    // Using parameterized query for security
    const [user] = await db.query(
        `SELECT * FROM users WHERE token = ?`,
        [token]
    );

    db.release();
    return user[0];  // Return the first matching user or undefined if none found
}

// Export functions
module.exports = {
    retrieveUserWithCredentials,
    retrieveUserWithToken
};