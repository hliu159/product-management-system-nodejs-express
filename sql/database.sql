-- Run this script to create or re-initialize the database.
drop table if exists products;
drop table if exists users;

CREATE TABLE "users" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "email" varchar(64) NOT NULL unique,
  "password" varchar(64) NOT NULL,
  "token" varchar(128) NOT NULL
);

create table "products"(
        "id" INTEGER PRIMARY KEY AUTOINCREMENT,
        "category_id" integer,
        "title" varchar(128),
        "description" varchar(128),
        "price" float(2),
        "product_image" varchar(128),
        "is_active" integer
);

INSERT INTO "users" VALUES
(1, "admin", "123456", "dJTsQnKv_UZqF2LeY");

INSERT into "products" (category_id, title, description, price, product_image, is_active) values
(1, "Lenovo ThinkPad", "E480", 6999.00, "images/9xpXIGs1C11XLityTu1mq9SftqGM28Hmrb3U9Ff3.png", 1),
(1,"fdfgd", "ttt", 31213.56,"images/75Pf6QHPqSxp3o3xsottYBXuMK1TSUrmBKTeRtsT.jpg",1),
(1,"eregf","dfdf",457685.39,"images/75Pf6QHPqSxp3o3xsottYBXuMK1TSUrmBKTeRtsT.jpg",1),
(1,"wqedfg","wertfhj",47654.23,"images/75Pf6QHPqSxp3o3xsottYBXuMK1TSUrmBKTeRtsT.jpg",1),
(1,"ert","wghj",494.67,"images/75Pf6QHPqSxp3o3xsottYBXuMK1TSUrmBKTeRtsT.jpg",1);



